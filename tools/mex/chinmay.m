[ signal, states, parameters, total_samples, file_samples ] = load_bcidat( 'chim4.dat');
signal(6500:end,:) = [];
drops = (diff(signal(:,9))>1);
drops = [drops;0];
%%figure;plot(drops);
drops_number = sum(drops);
total_packets = total_samples/57;
loss_percentage = drops_number/total_packets*100;
xax = 1:length(drops);
ch = 1;
%%plot(xax,signal(:,1).*int32(drops),'r+',xax,signal(:,1));
%hold on;
figure;plot(xax,signal(:,ch));
indx = find(drops);
%scatter(indx,(max(max(signal(:,1)))).*int32(drops(indx)),10,'r');
hold on;scatter(indx,signal(indx,ch),150,'r','x');
hold off;

signal3 = double(signal);

idrop = [indx-7 indx-6 indx-5 indx-4 indx-3 indx-2 indx-1 indx];
idrop = reshape(idrop', 1,8*length(indx));

for i = 1:8
rpl = repmat(signal3(indx+1,i),1,8);
rpl = reshape(rpl', 1,8*length(indx));

signal3(idrop,i) = rpl;
end


for j = 1:length(indx)
    
     signal4 = [signal3(1:indx(j),:); signal3(indx(j),:); signal3(indx(j)+1:end,:)];
end

%signal3(idrop,ch) = [ones(8,1).*];
%figure;plot(signal4(:,ch));
xxx = linspace(1,floor(length(signal4(:,ch))/250),length(signal4(:,ch)));
figure;plot(xxx,signal4(:,ch).*(0.2/(2^15-1))*1000*1000);
xlabel('Time (seconds)');
ylabel('microVolts')
%signal4 = signal3;
%signal4(idrop,ch) = [];
%signal3(idrop,ch) = 0;

%signal3([indx ;(indx-1) ;(indx-2)],ch) = mean(signal3(:,ch));
%signalmean = conv(signal3(:,ch),ones(20,1),'same')/20;
%signalmean(1:10,1) = mean(signalmean);
%signalmean(end-40:end,1) = mean(signalmean);
%signalmean(indx) = mean(signalmean);
%rr = signal3(:,8)-signalmean;

%d = fdesign.highpass('Fst,Fp,Ast,Ap',0.001,0.01,60,1);

%designmethods(d);
%Hd = design(d,'equiripple');
%fvtool(Hd);
%d=fdesign.highpass('N,Fc',10,0.1,250);
d = fdesign.highpass('Fst,Fp,Ast,Ap',0.0005,0.006,60,1);%('Fst,Fp,Ast,Ap',0.0005,0.12,60,1);
designmethods(d)

% only valid design method is FIR window method
Hd = design(d);
hl = filtlow;
%Hd = filt2;
% Display filter magnitude response
%fvtool(Hd);


y = (filter(Hd,signal4));

y = (filter(hl,y));
txx = linspace(1,length(y(1000:end,ch))/250,length(y(1000:end,ch)));
figure;plot(txx,y(1000:end,ch).*(0.2/(2^15-1))*1000*1000);
xlabel('Time (seconds)');
ylabel('microVolts')
%plot(signal(:,8));
%hold on;plot(signalmean,'r');
%plot(rr);
t = buffer(y(200:end),250);
figure;plot(mean(t(:,2:end)'),'r');

 aw = fft((y(500:end,ch)));
 axx = linspace(0,125,floor(length(aw)/2)+1);
 figure;plot(axx,abs(aw(1:(floor(length(aw)/2)+1),1)))
 
 figure;pwelch(y(500:end,ch))
