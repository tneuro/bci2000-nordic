
signal3 = double(signal);

ch = 1;
%signal3([indx ;(indx-1) ;(indx-2)],ch) = mean(signal3(:,ch));
signalmean = conv(signal3(:,ch),ones(20,1),'same')/20;
signalmean(1:10,1) = mean(signalmean);
signalmean(end-40:end,1) = mean(signalmean);
%signalmean(indx) = mean(signalmean);
rr = signal3(:,8)-signalmean;
d = fdesign.highpass('Fst,Fp,Ast,Ap',0.001,0.02,60,1);
%designmethods(d);
%Hd = design(d,'equiripple');
%fvtool(Hd);
%d=fdesign.highpass('N,Fc',10,0.1,250);
designmethods(d)
% only valid design method is FIR window method
Hd = design(d);
% Display filter magnitude response
fvtool(Hd);


y = filter(Hd,signalmean);

Fc    = 0.2;
N = 100;   % FIR filter order
Hf = fdesign.lowpass('N,Fc',N,Fc);
designmethods(Hf)
Hdd = design(Hf);
fvtool(Hdd);

y = filter(Hdd,y);

figure;plot(y(500:end,1));

%plot(signal(:,8));
%hold on;plot(signalmean,'r');
%plot(rr);
t = buffer(y(50:end),250);
figure;plot(mean(t(:,2:end)'),'r');