origFormat = get(0, 'format');
format('shortE');
[ signal, states, parameters, total_samples, file_samples ] = load_bcidat( 'Signal-11-11-noise-saline.dat');
signalreal = (2.4/(2^24-1)/12).*double(signal);

clc
disp('Vrms noise')
std(signalreal)
set(0,'format', origFormat);
figure;plot(signalreal(:,1:8))



%%

x = signalreal(:,1);
Fs = 500;
N = length(x);
xdft = fft(x);
xdft = xdft(1:N/2+1);
psdx = (1/(Fs*N)).*abs(xdft).^2;
psdx(2:end-1) = 2*psdx(2:end-1);
freq = 0:Fs/length(x):Fs/2;
figure;plot(freq,10*log10(psdx)); grid on;
title('Periodogram Using FFT');
xlabel('Frequency (Hz)'); ylabel('Power/Frequency (dB/Hz)');