rec01 = tsig(1:28540,:);
rec02 = tsig(30662:51915,:);
rec03 = tsig(55813:76669,:);
rec04 = tsig(79447:end,:);
figure;plot(rec01)

recfilt01 = sigfilt(1:28540,:);
recfilt02 = sigfilt(30662:51915,:);
recfilt03 = sigfilt(55813:76669,:);
recfilt04 = sigfilt(79447:end,:);
figure;plot(recfilt01)
%%
ch = 4;
Fs = 500;
datasig = recfilt01;
nfft = 2^nextpow2(length(datasig(:,ch)));
Pxx = abs(fft(datasig(:,ch), nfft)).^2/length(datasig(:,ch))/Fs;
Hpsd = dspdata.psd(Pxx(1:length(Pxx)/2), 'Fs', Fs);
figure;plot(Hpsd);

%%
ch = 1;
L = length(datasig);
figure;pwelch(double(datasig(:,ch)),'','');
figure;pwelch(double(datasig(:,ch)),hamming(256),'',2^13); 
[pxx,f] = pwelch(double(datasig(:,ch)),hamming(256),'',2^13,Fs);
figure;plot(f,10*log10(pxx));
xlabel('Hz'); ylabel('dB');
NFFT = 2^nextpow2(L);
Y = fft(double(datasig(:,ch)),NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
figure;plot(f,2*abs(Y(1:NFFT/2+1))) 
title('Single-Sided Amplitude Spectrum')
xlabel('Hz')