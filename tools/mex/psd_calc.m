 close all
 
 Fs = 1500;
 range = 350;     % pC
 pathStr = 'C:\Users\aghom_000\Dropbox\Flex PCB Electrodes\DDC Headstage\DDC232 Headstage Recordings\Saline-Recordings-6-19-14';
 filename = '1.5kHz-Range0-saline.mat';
 data = double(adcreads_sort);
 dataf = EEGbandstop(data, 59, 61, Fs);
 dataf = EEGbandstop(dataf, 119, 121, Fs);
 dataf = EEGbandstop(dataf, 179, 181, Fs);
 dataf = EEGbandstop(dataf, 239, 241, Fs);
 dataf = EEGbandstop(dataf, 299, 301, Fs);

% dataf = data;

dataf = EEGbandpass(dataf,3,300,Fs);
%dataf = EEGbandpass(dataf,3,300,Fs);

% discard edge effects
%dataf = dataf(:,round(3*Fs):end-round(3*Fs));
dataf = dataf(:,round(1.5*Fs):end-round(1.5 *Fs));

% meanRMSNoisemv = mean(std(dataf(2:30,:),0,2) * 1e3);
% 
% meanRMSNoisepC = meanRMSNoisemv / 2.8 * 9.6;

% disp(['Mean RMS noise in pC: ' num2str(meanRMSNoisepC)]);


figure;
which_channel = 33;
std(dataf(which_channel,:)) % .* 1e3

nfft = 2^nextpow2(length(dataf(which_channel,:)));
Pxx = abs(fft(dataf(which_channel,:), nfft)).^2/length(dataf(which_channel,:))/Fs;
Hpsd = dspdata.psd(Pxx(1:length(Pxx)/2), 'Fs', Fs);
plot(Hpsd);


figure;
which_channel = 36;
disp('PPT = ')
std(dataf(which_channel,:)) % .* 1e3
%mean(std(dataf(:,:)))

nfft = 2^nextpow2(length(dataf(which_channel,:)));
Pxx = abs(fft(dataf(which_channel,:), nfft)).^2/length(dataf(which_channel,:))/Fs;
Hpsd = dspdata.psd(Pxx(1:length(Pxx)/2), 'Fs', Fs);
plot(Hpsd);




signalType = 'NOISE';
startSec = 0.001;
endSec = 0.001;
save_fig = 'TRUE';
headstage_gain = 1e6 / ((1 / 2^20) * range * 1000);  % output in fC
%headstage_gain = 1e6 ;  % output in ppm
BncChannel = 1;
gain_working_threshold = 1500;

numChan = 8;
numRow = 8;
numCol = 8;

figure;plot(dataf');

%[signal, meanVal, medianVal]  = calcSignal(data, 'NOISE', 1, 1, numRow, numCol, 1, Fs, 'TRUE', pathStr, filename, 1, 1, 1);
[signal, meanVal, medianVal]  = calcSignal(dataf, signalType, startSec, endSec, numRow, numCol, numChan, Fs, save_fig, pathStr, filename, headstage_gain, BncChannel, gain_working_threshold);
%[signal, meanVal, medianVal]  = calcSignal(data, 'SIGNAL', 10, 10, numRow, numCol, 1, Fs, 'TRUE', pathStr, filename, 10, 5, 0.1);