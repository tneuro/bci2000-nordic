[ signal, states, parameters, total_samples, file_samples ] = load_bcidat( '12-11-2014-exp2-gain6.dat');
Fs = 500;
drops = (diff(signal(:,9))>1);
drops = [drops;0];
%%figure;plot(drops);
drops_number = sum(drops);
total_packets = total_samples/57;
loss_percentage = drops_number/total_packets*100;
xax = 1:length(drops);
%%plot(xax,signal(:,1).*int32(drops),'r+',xax,signal(:,1));
hold on;
plot(xax,signal(:,2));
indx = find(drops);
%scatter(indx,(max(max(signal(:,1)))).*int32(drops(indx)),10,'r');
scatter(indx,signal(indx,2),150,'r','x');
hold off;
%%


tsig = double(signal(:,1:8)).*(2.4/6/2^23);
figure;plot(tsig)
legend('Ch1','CH2','CH3','CH4','CH5','CH6','Ch7','CH8')
figure;plot(diff(signal(:,9)));
sigfilt = EEGbandpass(tsig',1,200,Fs)';%1-200

figure;plot(sigfilt)
legend('Ch1','CH2','CH3','CH4','CH5','CH6','Ch7','CH8')
std(sigfilt)
figure;plot(diff(signal(:,9))>1);
%%

