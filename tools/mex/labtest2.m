[ signal, states, parameters, total_samples, file_samples ] = load_bcidat( '4Hz-2.dat');
drops = (diff(signal(:,9))>1);
drops = [drops;0];
%%figure;plot(drops);
drops_number = sum(drops);
total_packets = total_samples/57;
loss_percentage = drops_number/total_packets*100;
xax = 1:length(drops);
%%plot(xax,signal(:,1).*int32(drops),'r+',xax,signal(:,1));
%hold on;
figure;plot(xax,signal(:,1));
indx = find(drops);
%scatter(indx,(max(max(signal(:,1)))).*int32(drops(indx)),10,'r');
hold on;scatter(indx,signal(indx,2),150,'r','x');
hold off;

signal3 = double(signal);

ch = 1;
%signal3([indx ;(indx-1) ;(indx-2)],ch) = mean(signal3(:,ch));
%signalmean = conv(signal3(:,ch),ones(20,1),'same')/20;
%signalmean(1:10,1) = mean(signalmean);
%signalmean(end-40:end,1) = mean(signalmean);
%signalmean(indx) = mean(signalmean);
%rr = signal3(:,8)-signalmean;
d = fdesign.highpass('Fst,Fp,Ast,Ap',0.0005,0.01,60,1);
%designmethods(d);
%Hd = design(d,'equiripple');
%fvtool(Hd);
%d=fdesign.highpass('N,Fc',10,0.1,250);
designmethods(d)
% only valid design method is FIR window method
Hd = design(d);
% Display filter magnitude response
fvtool(Hd);


y = abs(filter(Hd,signal3));


figure;plot(y(500:end,1));

%plot(signal(:,8));
%hold on;plot(signalmean,'r');
%plot(rr);
t = buffer(y(50:end),250);
figure;plot(mean(t(:,2:end)'),'r');

 aw = fft((y(500:end,1)));
  axx = linspace(0,125,floor(length(aw)/2)+1);
 figure;plot(axx,abs(aw(1:(floor(length(aw)/2)+1),1)))
 aq = 250/length(aw)