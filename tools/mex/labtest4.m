[ signal, states, parameters, total_samples, file_samples ] = load_bcidat( '4Hz-2.dat');

ch = 1;

drops = (diff(signal(:,9))>1);
drops = [drops;0];

drops_number = sum(drops);
total_packets = total_samples/57;
loss_percentage = drops_number/total_packets*100;
xax = 1:length(drops);

figure;plot(xax,signal(:,ch));
indx = find(drops);

hold on;scatter(indx,signal(indx,ch),150,'r','x');
hold off;

signal3 = double(signal);



d = fdesign.highpass('Fst,Fp,Ast,Ap',0.009,0.03,60,1);%('Fst,Fp,Ast,Ap',0.0005,0.01,60,1);

designmethods(d)
Hd = design(d);
hl = filtlow;
fvtool(Hd);


y = (filter(Hd,signal3));
y = (filter(hl,y));


txx = linspace(1,length(y(500:end,ch))/250,length(y(500:end,ch)));
figure;plot(txx,detrend(y(500:end,ch).*(0.2/(2^15-1))*1000*1000));

xlabel('Time (seconds)');
ylabel('microVolts')

t = buffer(y(50:end),250);
figure;plot(mean(t(:,2:end)'),'r');

aw = fft((y(500:end,ch)));
aw(1) = 0;
axx = linspace(0,125,floor(length(aw)/2)+1);
figure;plot(axx,abs(aw(1:(floor(length(aw)/2)+1),1)))

meanaw = mean(abs(aw));
aw2 = (abs(aw)).*(abs(aw)>(meanaw+14290));
pks = (aw2>[0;aw2(1:end-1)]) .* (aw2>=[aw2(2:end);0]);
dx = find(pks(1:floor(length(aw)/2+1))>0);

peaks = pks(dx).* abs(aw(dx));
hold on; plot(axx,meanaw.*ones(floor(length(aw)/2+1),1)+14290,'r')
hold on;plot(axx(dx),peaks,'ks','markeredgecolor',[0 0 0]);
xlabel('Frequency (Hz)');
title(['Channel ' num2str(ch)])

figure;pwelch(y(500:end,ch))