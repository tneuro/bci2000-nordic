[ signal, states, parameters, total_samples, file_samples ] = load_bcidat( 'Noise2.dat');
%figure;plot(signal(:,1:8))
close all;
sign_main = double(signal(:,1:8)).*2.4./2^23/12.*1000000;
%sign_main = double(signal(:,1:8)).*2.4./2^24/12;
xx = linspace(0,length(signal)/500,length(signal));
figure;plot(xx,sign_main);
%8470
hold on;
onn = ones(length(signal),8);
means = mean (sign_main);
nsign = repmat(means,length(sign_main),1);
%plot(xx,nsign)
xlabel('seconds')
ylabel('uV')
title('Ch = 1-8')
legend('Ch1','Ch2','Ch3','Ch4','Ch5','Ch6','Ch7','Ch8')
hold off;
mean(means)
Fs = 500;
dataf = EEGbandpass(sign_main',1,200,Fs);

%dataf = dataf(:,round(3*401):end-round(3 *401));
dataf = dataf(:,round(3*Fs):end-round(3 *Fs));
xx = linspace(0,length(dataf)/500,length(dataf));
figure;plot(xx,dataf');
hold on;
%plot((signal(:,10)-10))
title('Ch = 1-8 Band passed 1-200Hz')
legend('Ch1','Ch2','Ch3','Ch4','Ch5','Ch6','Ch7','Ch8')
xlabel('Seconds')
ylabel('uV')
hold off;



%%
%close all
sig_filt = dataf';
trig = signal(:,10);
trigfilt = signal(round(3*Fs):end-round(3 *Fs),10);
%figure; plot(trig)
trig2 = (trig>5);
trigfilt2 = (trigfilt>5);
trig_index = (diff(trig2)>0); 
trigfilt_index = (diff(trigfilt2)>0); 
indx = find(trig_index);
indxfilt = find(trigfilt_index);


dix = diff(indx);
dnn = find(dix<100);
om_indx = indx(dnn+1);
trig_index(om_indx) = 0;
indx = find(trig_index);


dixfilt = diff(indxfilt);
dnnfilt = find(dixfilt<100);
om_indxfilt = indxfilt(dnnfilt+1);
trigfilt_index(om_indxfilt) = 0;
indxfilt = find(trigfilt_index);





for j = 1:8
    for i = 1:length(indx)
        new_shift1(:,i,j) = sig_filt(indxfilt(i):indxfilt(i)+401,j);
        new_shift2(:,i,j) = sign_main(indx(i):indx(i)+401,j);
        
    end
end
ch = 8;
xx = linspace(0,402/500*1000,402);
%figure;plot(xx,new_shift1(:,:,ch))
%figure;plot(xx,new_shift2(:,:,ch))
%figure;plot(xx,mean(new_shift2(:,:,ch)'))
%figure;plot(xx,mean(new_shift1(:,:,ch)'))


tss = mean(new_shift1(:,:,:),2);
trs(:,:) = tss;
figure;plot(xx,trs);
xlabel('mSeconds')
ylabel('uV')
title('Ch = 1-8')
legend('Ch1','Ch2','Ch3','Ch4','Ch5','Ch6','Ch7','Ch8')

%%


figure;
subplot(4,2,1);
plot(xx,mean(new_shift1(:,:,1)'))
xlabel('mSeconds')
ylabel('uV')
title('Ch = 1')


subplot(4,2,2);
plot(xx,mean(new_shift1(:,:,2)'))
xlabel('mSeconds')
ylabel('uV')
title('Ch = 2')


subplot(4,2,3);
plot(xx,mean(new_shift1(:,:,3)'))
xlabel('mSeconds')
ylabel('uV')
title('Ch = 3')


subplot(4,2,4);
plot(xx,mean(new_shift1(:,:,4)'))
xlabel('mSeconds')
ylabel('uV')
title('Ch = 4')


subplot(4,2,5);
plot(xx,mean(new_shift1(:,:,5)'))
xlabel('mSeconds')
ylabel('uV')
title('Ch = 5')


subplot(4,2,6);
plot(xx,mean(new_shift1(:,:,6)'))
xlabel('mSeconds')
ylabel('uV')
title('Ch = 6')


subplot(4,2,7);
plot(xx,mean(new_shift1(:,:,7)'))
xlabel('mSeconds')
ylabel('uV')
title('Ch = 7')


subplot(4,2,8);
plot(xx,mean(new_shift1(:,:,8)'))
xlabel('mSeconds')
ylabel('uV')
title('Ch = 8')


%%

xx = linspace(0,length(dataf)/500,length(dataf));
figure;plot(xx,dataf');
title('Ch = 1-8 Band passed 1-200Hz')
legend('Ch1','Ch2','Ch3','Ch4','Ch5','Ch6','Ch7','Ch8')
xlabel('Seconds')
ylabel('uV')



xc = repmat(indxfilt,1,2)'./500;
yc = repmat(ylim,length(indxfilt),1)';
line(xc,yc)