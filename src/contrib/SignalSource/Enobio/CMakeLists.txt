###########################################################################
## $Id: CMakeLists.txt 4096 2012-06-11 11:43:15Z mellinger $
## Authors: masayo.haneda@starlab.es
## Description: Build information for ENOBIO

ADD_SUBDIRECTORY( Enobio )
#ADD_SUBDIRECTORY( Enobio3G )
