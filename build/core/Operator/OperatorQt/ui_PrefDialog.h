/********************************************************************************
** Form generated from reading UI file 'PrefDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PREFDIALOG_H
#define UI_PREFDIALOG_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>

QT_BEGIN_NAMESPACE

class Ui_PrefDialog
{
public:
    QGroupBox *groupBox;
    QLabel *label_3;
    QLineEdit *edit_OnConnect;
    QLineEdit *edit_OnExit;
    QLabel *label_4;
    QLineEdit *edit_OnSetConfig;
    QLabel *label_5;
    QLineEdit *edit_OnResume;
    QLabel *label_6;
    QLineEdit *edit_OnSuspend;
    QLabel *label_13;
    QLineEdit *edit_OnStart;
    QLabel *label_14;
    QPushButton *okButton;
    QSlider *slider_UserLevel;
    QLabel *label;
    QLabel *label_UserLevel;
    QGroupBox *groupBox_3;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLineEdit *edit_NameBtn1;
    QLineEdit *edit_CmdBtn1;
    QLineEdit *edit_CmdBtn2;
    QLineEdit *edit_NameBtn2;
    QLineEdit *edit_CmdBtn3;
    QLineEdit *edit_NameBtn3;
    QLineEdit *edit_CmdBtn4;
    QLineEdit *edit_NameBtn4;
    QLabel *label_9;
    QPushButton *cancelButton;

    void setupUi(QDialog *PrefDialog)
    {
        if (PrefDialog->objectName().isEmpty())
            PrefDialog->setObjectName(QString::fromUtf8("PrefDialog"));
        PrefDialog->resize(503, 348);
        PrefDialog->setMinimumSize(QSize(503, 348));
        PrefDialog->setMaximumSize(QSize(503, 348));
        QFont font;
        font.setFamily(QString::fromUtf8("MS Sans Serif"));
        PrefDialog->setFont(font);
        PrefDialog->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        groupBox = new QGroupBox(PrefDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 221, 331));
        QFont font1;
        font1.setFamily(QString::fromUtf8("MS Sans Serif"));
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        groupBox->setFont(font1);
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 30, 131, 16));
        QFont font2;
        font2.setPointSize(8);
        font2.setBold(false);
        font2.setWeight(50);
        label_3->setFont(font2);
        edit_OnConnect = new QLineEdit(groupBox);
        edit_OnConnect->setObjectName(QString::fromUtf8("edit_OnConnect"));
        edit_OnConnect->setGeometry(QRect(10, 50, 201, 20));
        edit_OnConnect->setFont(font2);
        edit_OnExit = new QLineEdit(groupBox);
        edit_OnExit->setObjectName(QString::fromUtf8("edit_OnExit"));
        edit_OnExit->setGeometry(QRect(10, 100, 201, 20));
        edit_OnExit->setFont(font2);
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 80, 131, 16));
        label_4->setFont(font2);
        edit_OnSetConfig = new QLineEdit(groupBox);
        edit_OnSetConfig->setObjectName(QString::fromUtf8("edit_OnSetConfig"));
        edit_OnSetConfig->setGeometry(QRect(10, 150, 201, 20));
        edit_OnSetConfig->setFont(font2);
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(10, 130, 131, 16));
        label_5->setFont(font2);
        edit_OnResume = new QLineEdit(groupBox);
        edit_OnResume->setObjectName(QString::fromUtf8("edit_OnResume"));
        edit_OnResume->setGeometry(QRect(10, 200, 201, 20));
        edit_OnResume->setFont(font2);
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 180, 131, 16));
        label_6->setFont(font2);
        edit_OnSuspend = new QLineEdit(groupBox);
        edit_OnSuspend->setObjectName(QString::fromUtf8("edit_OnSuspend"));
        edit_OnSuspend->setGeometry(QRect(10, 250, 201, 20));
        edit_OnSuspend->setFont(font2);
        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(10, 230, 131, 16));
        label_13->setFont(font2);
        edit_OnStart = new QLineEdit(groupBox);
        edit_OnStart->setObjectName(QString::fromUtf8("edit_OnStart"));
        edit_OnStart->setGeometry(QRect(10, 300, 201, 20));
        edit_OnStart->setFont(font2);
        label_14 = new QLabel(groupBox);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(10, 280, 131, 16));
        label_14->setFont(font2);
        okButton = new QPushButton(PrefDialog);
        okButton->setObjectName(QString::fromUtf8("okButton"));
        okButton->setGeometry(QRect(410, 310, 75, 24));
        okButton->setAutoDefault(false);
        okButton->setDefault(true);
        slider_UserLevel = new QSlider(PrefDialog);
        slider_UserLevel->setObjectName(QString::fromUtf8("slider_UserLevel"));
        slider_UserLevel->setGeometry(QRect(420, 240, 51, 21));
        slider_UserLevel->setMinimum(1);
        slider_UserLevel->setMaximum(3);
        slider_UserLevel->setPageStep(2);
        slider_UserLevel->setOrientation(Qt::Horizontal);
        label = new QLabel(PrefDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(410, 220, 81, 16));
        QFont font3;
        font3.setPointSize(10);
        font3.setBold(true);
        font3.setWeight(75);
        label->setFont(font3);
        label_UserLevel = new QLabel(PrefDialog);
        label_UserLevel->setObjectName(QString::fromUtf8("label_UserLevel"));
        label_UserLevel->setGeometry(QRect(410, 270, 71, 16));
        label_UserLevel->setAlignment(Qt::AlignCenter);
        groupBox_3 = new QGroupBox(PrefDialog);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(240, 10, 251, 171));
        groupBox_3->setFont(font1);
        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(10, 52, 61, 16));
        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(10, 80, 61, 21));
        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(10, 140, 61, 20));
        label_11 = new QLabel(groupBox_3);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(70, 30, 41, 16));
        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(140, 30, 71, 16));
        edit_NameBtn1 = new QLineEdit(groupBox_3);
        edit_NameBtn1->setObjectName(QString::fromUtf8("edit_NameBtn1"));
        edit_NameBtn1->setGeometry(QRect(70, 50, 61, 20));
        edit_NameBtn1->setFont(font2);
        edit_CmdBtn1 = new QLineEdit(groupBox_3);
        edit_CmdBtn1->setObjectName(QString::fromUtf8("edit_CmdBtn1"));
        edit_CmdBtn1->setGeometry(QRect(140, 50, 101, 20));
        edit_CmdBtn1->setFont(font2);
        edit_CmdBtn2 = new QLineEdit(groupBox_3);
        edit_CmdBtn2->setObjectName(QString::fromUtf8("edit_CmdBtn2"));
        edit_CmdBtn2->setGeometry(QRect(140, 80, 101, 20));
        edit_CmdBtn2->setFont(font2);
        edit_NameBtn2 = new QLineEdit(groupBox_3);
        edit_NameBtn2->setObjectName(QString::fromUtf8("edit_NameBtn2"));
        edit_NameBtn2->setGeometry(QRect(70, 80, 61, 20));
        edit_NameBtn2->setFont(font2);
        edit_CmdBtn3 = new QLineEdit(groupBox_3);
        edit_CmdBtn3->setObjectName(QString::fromUtf8("edit_CmdBtn3"));
        edit_CmdBtn3->setGeometry(QRect(140, 110, 101, 20));
        edit_CmdBtn3->setFont(font2);
        edit_NameBtn3 = new QLineEdit(groupBox_3);
        edit_NameBtn3->setObjectName(QString::fromUtf8("edit_NameBtn3"));
        edit_NameBtn3->setGeometry(QRect(70, 110, 61, 20));
        edit_NameBtn3->setFont(font2);
        edit_CmdBtn4 = new QLineEdit(groupBox_3);
        edit_CmdBtn4->setObjectName(QString::fromUtf8("edit_CmdBtn4"));
        edit_CmdBtn4->setGeometry(QRect(140, 140, 101, 20));
        edit_CmdBtn4->setFont(font2);
        edit_NameBtn4 = new QLineEdit(groupBox_3);
        edit_NameBtn4->setObjectName(QString::fromUtf8("edit_NameBtn4"));
        edit_NameBtn4->setGeometry(QRect(70, 140, 61, 20));
        edit_NameBtn4->setFont(font2);
        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(10, 110, 61, 21));
        cancelButton = new QPushButton(PrefDialog);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));
        cancelButton->setGeometry(QRect(320, 310, 75, 24));
        cancelButton->setAutoDefault(false);
        cancelButton->setDefault(false);
        QWidget::setTabOrder(edit_OnConnect, edit_OnExit);
        QWidget::setTabOrder(edit_OnExit, edit_OnSetConfig);
        QWidget::setTabOrder(edit_OnSetConfig, edit_OnResume);
        QWidget::setTabOrder(edit_OnResume, edit_OnSuspend);
        QWidget::setTabOrder(edit_OnSuspend, edit_OnStart);
        QWidget::setTabOrder(edit_OnStart, edit_NameBtn1);
        QWidget::setTabOrder(edit_NameBtn1, edit_CmdBtn1);
        QWidget::setTabOrder(edit_CmdBtn1, edit_NameBtn2);
        QWidget::setTabOrder(edit_NameBtn2, edit_CmdBtn2);
        QWidget::setTabOrder(edit_CmdBtn2, edit_NameBtn3);
        QWidget::setTabOrder(edit_NameBtn3, edit_CmdBtn3);
        QWidget::setTabOrder(edit_CmdBtn3, edit_NameBtn4);
        QWidget::setTabOrder(edit_NameBtn4, edit_CmdBtn4);
        QWidget::setTabOrder(edit_CmdBtn4, slider_UserLevel);
        QWidget::setTabOrder(slider_UserLevel, okButton);

        retranslateUi(PrefDialog);

        QMetaObject::connectSlotsByName(PrefDialog);
    } // setupUi

    void retranslateUi(QDialog *PrefDialog)
    {
        PrefDialog->setWindowTitle(QApplication::translate("PrefDialog", "Preferences", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("PrefDialog", "Script Files", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("PrefDialog", "After all modules connected", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("PrefDialog", "On Exit of BCI2000", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("PrefDialog", "On SetConfig", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("PrefDialog", "On Resume", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("PrefDialog", "On Suspend", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("PrefDialog", "On Start", 0, QApplication::UnicodeUTF8));
        okButton->setText(QApplication::translate("PrefDialog", "OK", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("PrefDialog", "User Level", 0, QApplication::UnicodeUTF8));
        label_UserLevel->setText(QApplication::translate("PrefDialog", "Beginner", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("PrefDialog", "Function Buttons", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("PrefDialog", "Button 1", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("PrefDialog", "Button 2", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("PrefDialog", "Button 4", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("PrefDialog", "Name", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("PrefDialog", "Command", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("PrefDialog", "Button 3", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("PrefDialog", "Cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class PrefDialog: public Ui_PrefDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PREFDIALOG_H
