/********************************************************************************
** Form generated from reading UI file 'ShowParameters.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOWPARAMETERS_H
#define UI_SHOWPARAMETERS_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ShowParameters
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTreeWidget *treeWidget;

    void setupUi(QDialog *ShowParameters)
    {
        if (ShowParameters->objectName().isEmpty())
            ShowParameters->setObjectName(QString::fromUtf8("ShowParameters"));
        ShowParameters->resize(249, 469);
        ShowParameters->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        verticalLayout = new QVBoxLayout(ShowParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(ShowParameters);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        verticalLayout->addWidget(label);

        treeWidget = new QTreeWidget(ShowParameters);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        treeWidget->setHeaderItem(__qtreewidgetitem);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        treeWidget->setColumnCount(1);
        treeWidget->header()->setVisible(false);

        verticalLayout->addWidget(treeWidget);


        retranslateUi(ShowParameters);

        QMetaObject::connectSlotsByName(ShowParameters);
    } // setupUi

    void retranslateUi(QDialog *ShowParameters)
    {
        ShowParameters->setWindowTitle(QApplication::translate("ShowParameters", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ShowParameters", "TextLabel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ShowParameters: public Ui_ShowParameters {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOWPARAMETERS_H
