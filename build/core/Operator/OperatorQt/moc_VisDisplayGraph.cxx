/****************************************************************************
** Meta object code from reading C++ file 'VisDisplayGraph.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/core/Operator/OperatorQt/VisDisplayGraph.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'VisDisplayGraph.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_VisDisplayGraph[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      28,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x0a,
      45,   16,   16,   16, 0x08,
      65,   16,   16,   16, 0x08,
      81,   16,   16,   16, 0x08,
      96,   16,   16,   16, 0x08,
     114,   16,   16,   16, 0x08,
     129,   16,   16,   16, 0x08,
     143,   16,   16,   16, 0x08,
     158,   16,   16,   16, 0x08,
     174,   16,   16,   16, 0x08,
     194,   16,   16,   16, 0x08,
     216,   16,   16,   16, 0x08,
     234,   16,   16,   16, 0x08,
     252,   16,   16,   16, 0x08,
     274,   16,   16,   16, 0x08,
     288,   16,   16,   16, 0x08,
     304,   16,   16,   16, 0x08,
     319,   16,   16,   16, 0x08,
     330,   16,   16,   16, 0x08,
     340,   16,   16,   16, 0x08,
     349,   16,   16,   16, 0x08,
     358,   16,   16,   16, 0x08,
     369,   16,   16,   16, 0x08,
     379,   16,   16,   16, 0x08,
     389,   16,   16,   16, 0x08,
     399,   16,   16,   16, 0x08,
     413,   16,   16,   16, 0x08,
     426,   16,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_VisDisplayGraph[] = {
    "VisDisplayGraph\0\0HandleSignal(GenericSignal)\0"
    "ContextMenu(QPoint)\0EnlargeSignal()\0"
    "ReduceSignal()\0ToggleAutoScale()\0"
    "FewerSamples()\0MoreSamples()\0"
    "MoreChannels()\0FewerChannels()\0"
    "ToggleDisplayMode()\0ToggleNumericValues()\0"
    "ToggleBaselines()\0ToggleValueUnit()\0"
    "ToggleChannelLabels()\0ToggleColor()\0"
    "InvertDisplay()\0ChooseColors()\0"
    "SetHPOff()\0SetHP01()\0SetHP1()\0SetHP5()\0"
    "SetLPOff()\0SetLP30()\0SetLP40()\0SetLP70()\0"
    "SetNotchOff()\0SetNotch50()\0SetNotch60()\0"
};

void VisDisplayGraph::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VisDisplayGraph *_t = static_cast<VisDisplayGraph *>(_o);
        switch (_id) {
        case 0: _t->HandleSignal((*reinterpret_cast< const GenericSignal(*)>(_a[1]))); break;
        case 1: _t->ContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 2: _t->EnlargeSignal(); break;
        case 3: _t->ReduceSignal(); break;
        case 4: _t->ToggleAutoScale(); break;
        case 5: _t->FewerSamples(); break;
        case 6: _t->MoreSamples(); break;
        case 7: _t->MoreChannels(); break;
        case 8: _t->FewerChannels(); break;
        case 9: _t->ToggleDisplayMode(); break;
        case 10: _t->ToggleNumericValues(); break;
        case 11: _t->ToggleBaselines(); break;
        case 12: _t->ToggleValueUnit(); break;
        case 13: _t->ToggleChannelLabels(); break;
        case 14: _t->ToggleColor(); break;
        case 15: _t->InvertDisplay(); break;
        case 16: _t->ChooseColors(); break;
        case 17: _t->SetHPOff(); break;
        case 18: _t->SetHP01(); break;
        case 19: _t->SetHP1(); break;
        case 20: _t->SetHP5(); break;
        case 21: _t->SetLPOff(); break;
        case 22: _t->SetLP30(); break;
        case 23: _t->SetLP40(); break;
        case 24: _t->SetLP70(); break;
        case 25: _t->SetNotchOff(); break;
        case 26: _t->SetNotch50(); break;
        case 27: _t->SetNotch60(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData VisDisplayGraph::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VisDisplayGraph::staticMetaObject = {
    { &VisDisplayLayer::staticMetaObject, qt_meta_stringdata_VisDisplayGraph,
      qt_meta_data_VisDisplayGraph, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VisDisplayGraph::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VisDisplayGraph::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VisDisplayGraph::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VisDisplayGraph))
        return static_cast<void*>(const_cast< VisDisplayGraph*>(this));
    return VisDisplayLayer::qt_metacast(_clname);
}

int VisDisplayGraph::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = VisDisplayLayer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
