/****************************************************************************
** Meta object code from reading C++ file 'VisDisplayBitmap.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/core/Operator/OperatorQt/VisDisplayBitmap.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'VisDisplayBitmap.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_VisDisplayBitmap[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_VisDisplayBitmap[] = {
    "VisDisplayBitmap\0\0HandleBitmap(BitmapImage)\0"
};

void VisDisplayBitmap::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VisDisplayBitmap *_t = static_cast<VisDisplayBitmap *>(_o);
        switch (_id) {
        case 0: _t->HandleBitmap((*reinterpret_cast< const BitmapImage(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData VisDisplayBitmap::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VisDisplayBitmap::staticMetaObject = {
    { &VisDisplayLayer::staticMetaObject, qt_meta_stringdata_VisDisplayBitmap,
      qt_meta_data_VisDisplayBitmap, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VisDisplayBitmap::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VisDisplayBitmap::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VisDisplayBitmap::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VisDisplayBitmap))
        return static_cast<void*>(const_cast< VisDisplayBitmap*>(this));
    return VisDisplayLayer::qt_metacast(_clname);
}

int VisDisplayBitmap::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = VisDisplayLayer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
