/********************************************************************************
** Form generated from reading UI file 'ShowStates.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOWSTATES_H
#define UI_SHOWSTATES_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>

QT_BEGIN_NAMESPACE

class Ui_ShowStates
{
public:
    QHBoxLayout *horizontalLayout;
    QListWidget *listWidget;

    void setupUi(QDialog *ShowStates)
    {
        if (ShowStates->objectName().isEmpty())
            ShowStates->setObjectName(QString::fromUtf8("ShowStates"));
        ShowStates->resize(226, 350);
        horizontalLayout = new QHBoxLayout(ShowStates);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        listWidget = new QListWidget(ShowStates);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));

        horizontalLayout->addWidget(listWidget);


        retranslateUi(ShowStates);

        QMetaObject::connectSlotsByName(ShowStates);
    } // setupUi

    void retranslateUi(QDialog *ShowStates)
    {
        ShowStates->setWindowTitle(QApplication::translate("ShowStates", "Dialog", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ShowStates: public Ui_ShowStates {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOWSTATES_H
