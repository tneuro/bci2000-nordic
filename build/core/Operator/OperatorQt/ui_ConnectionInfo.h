/********************************************************************************
** Form generated from reading UI file 'ConnectionInfo.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONNECTIONINFO_H
#define UI_CONNECTIONINFO_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_ConnectionInfo
{
public:
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_Source;
    QLabel *label_SigProc;
    QLabel *label_App;

    void setupUi(QDialog *ConnectionInfo)
    {
        if (ConnectionInfo->objectName().isEmpty())
            ConnectionInfo->setObjectName(QString::fromUtf8("ConnectionInfo"));
        ConnectionInfo->resize(553, 215);
        ConnectionInfo->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        label = new QLabel(ConnectionInfo);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 51, 16));
        QFont font;
        font.setFamily(QString::fromUtf8("MS Sans Serif"));
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label_2 = new QLabel(ConnectionInfo);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(190, 10, 121, 16));
        label_2->setFont(font);
        label_3 = new QLabel(ConnectionInfo);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(380, 10, 81, 16));
        label_3->setFont(font);
        label_Source = new QLabel(ConnectionInfo);
        label_Source->setObjectName(QString::fromUtf8("label_Source"));
        label_Source->setGeometry(QRect(10, 40, 161, 171));
        label_Source->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_SigProc = new QLabel(ConnectionInfo);
        label_SigProc->setObjectName(QString::fromUtf8("label_SigProc"));
        label_SigProc->setGeometry(QRect(190, 40, 161, 171));
        label_SigProc->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_App = new QLabel(ConnectionInfo);
        label_App->setObjectName(QString::fromUtf8("label_App"));
        label_App->setGeometry(QRect(380, 40, 161, 171));
        label_App->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        retranslateUi(ConnectionInfo);

        QMetaObject::connectSlotsByName(ConnectionInfo);
    } // setupUi

    void retranslateUi(QDialog *ConnectionInfo)
    {
        ConnectionInfo->setWindowTitle(QApplication::translate("ConnectionInfo", "Connection Info", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ConnectionInfo", "Source", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("ConnectionInfo", "SignalProcessing", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("ConnectionInfo", "Application", 0, QApplication::UnicodeUTF8));
        label_Source->setText(QApplication::translate("ConnectionInfo", "N/A", 0, QApplication::UnicodeUTF8));
        label_SigProc->setText(QApplication::translate("ConnectionInfo", "N/A", 0, QApplication::UnicodeUTF8));
        label_App->setText(QApplication::translate("ConnectionInfo", "N/A", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ConnectionInfo: public Ui_ConnectionInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONNECTIONINFO_H
