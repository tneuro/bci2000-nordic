/****************************************************************************
** Meta object code from reading C++ file 'MainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/core/Operator/OperatorQt/MainWindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      40,   11,   11,   11, 0x08,
      67,   11,   11,   11, 0x08,
     101,   11,   11,   11, 0x08,
     137,   11,   11,   11, 0x08,
     176,   11,   11,   11, 0x08,
     209,   11,   11,   11, 0x08,
     240,   11,   11,   11, 0x08,
     274,   11,   11,   11, 0x08,
     308,   11,   11,   11, 0x08,
     334,   11,   11,   11, 0x08,
     363,   11,   11,   11, 0x08,
     392,   11,   11,   11, 0x08,
     421,   11,   11,   11, 0x08,
     450,   11,   11,   11, 0x08,
     479,   11,   11,   11, 0x08,
     517,  500,   11,   11, 0x0a,
     559,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0on_actionStates_triggered()\0"
    "on_actionAbout_triggered()\0"
    "on_actionBCI2000_Help_triggered()\0"
    "on_actionOperator_Log_toggled(bool)\0"
    "on_actionConnection_Info_toggled(bool)\0"
    "on_actionPreferences_triggered()\0"
    "on_pushButton_Config_clicked()\0"
    "on_pushButton_SetConfig_clicked()\0"
    "on_pushButton_RunSystem_clicked()\0"
    "on_actionQuit_triggered()\0"
    "on_pushButton_Quit_clicked()\0"
    "on_pushButton_Btn1_clicked()\0"
    "on_pushButton_Btn2_clicked()\0"
    "on_pushButton_Btn3_clicked()\0"
    "on_pushButton_Btn4_clicked()\0"
    "CloseTopmostWindow()\0idx,title,script\0"
    "SetFunctionButton(size_t,QString,QString)\0"
    "PutParameters()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->on_actionStates_triggered(); break;
        case 1: _t->on_actionAbout_triggered(); break;
        case 2: _t->on_actionBCI2000_Help_triggered(); break;
        case 3: _t->on_actionOperator_Log_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->on_actionConnection_Info_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_actionPreferences_triggered(); break;
        case 6: _t->on_pushButton_Config_clicked(); break;
        case 7: _t->on_pushButton_SetConfig_clicked(); break;
        case 8: _t->on_pushButton_RunSystem_clicked(); break;
        case 9: _t->on_actionQuit_triggered(); break;
        case 10: _t->on_pushButton_Quit_clicked(); break;
        case 11: _t->on_pushButton_Btn1_clicked(); break;
        case 12: _t->on_pushButton_Btn2_clicked(); break;
        case 13: _t->on_pushButton_Btn3_clicked(); break;
        case 14: _t->on_pushButton_Btn4_clicked(); break;
        case 15: _t->CloseTopmostWindow(); break;
        case 16: _t->SetFunctionButton((*reinterpret_cast< size_t(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 17: _t->PutParameters(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
