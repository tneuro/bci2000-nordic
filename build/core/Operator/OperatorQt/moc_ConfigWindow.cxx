/****************************************************************************
** Meta object code from reading C++ file 'ConfigWindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/core/Operator/OperatorQt/ConfigWindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ConfigWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ConfigWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x08,
      38,   13,   13,   13, 0x08,
      48,   13,   13,   13, 0x08,
      72,   13,   13,   13, 0x08,
      96,   13,   13,   13, 0x08,
     125,   13,   13,   13, 0x08,
     154,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ConfigWindow[] = {
    "ConfigWindow\0\0OnCfgTabControlChange()\0"
    "OnClose()\0OnSaveParametersClick()\0"
    "OnLoadParametersClick()\0"
    "OnConfigureSaveFilterClick()\0"
    "OnConfigureLoadFilterClick()\0OnHelpClick()\0"
};

void ConfigWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ConfigWindow *_t = static_cast<ConfigWindow *>(_o);
        switch (_id) {
        case 0: _t->OnCfgTabControlChange(); break;
        case 1: _t->OnClose(); break;
        case 2: _t->OnSaveParametersClick(); break;
        case 3: _t->OnLoadParametersClick(); break;
        case 4: _t->OnConfigureSaveFilterClick(); break;
        case 5: _t->OnConfigureLoadFilterClick(); break;
        case 6: _t->OnHelpClick(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ConfigWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ConfigWindow::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ConfigWindow,
      qt_meta_data_ConfigWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ConfigWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ConfigWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ConfigWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ConfigWindow))
        return static_cast<void*>(const_cast< ConfigWindow*>(this));
    return QDialog::qt_metacast(_clname);
}

int ConfigWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
