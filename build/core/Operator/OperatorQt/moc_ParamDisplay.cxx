/****************************************************************************
** Meta object code from reading C++ file 'ParamDisplay.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/core/Operator/OperatorQt/ParamDisplay.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ParamDisplay.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ParamLabel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_ParamLabel[] = {
    "ParamLabel\0"
};

void ParamLabel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ParamLabel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ParamLabel::staticMetaObject = {
    { &QLabel::staticMetaObject, qt_meta_stringdata_ParamLabel,
      qt_meta_data_ParamLabel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ParamLabel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ParamLabel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ParamLabel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ParamLabel))
        return static_cast<void*>(const_cast< ParamLabel*>(this));
    return QLabel::qt_metacast(_clname);
}

int ParamLabel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLabel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_DisplayBase[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DisplayBase[] = {
    "DisplayBase\0\0OnContentChange()\0"
};

void DisplayBase::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DisplayBase *_t = static_cast<DisplayBase *>(_o);
        switch (_id) {
        case 0: _t->OnContentChange(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData DisplayBase::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DisplayBase::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_DisplayBase,
      qt_meta_data_DisplayBase, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DisplayBase::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DisplayBase::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DisplayBase::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DisplayBase))
        return static_cast<void*>(const_cast< DisplayBase*>(this));
    return QObject::qt_metacast(_clname);
}

int DisplayBase::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_SeparateComment[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_SeparateComment[] = {
    "SeparateComment\0"
};

void SeparateComment::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SeparateComment::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SeparateComment::staticMetaObject = {
    { &DisplayBase::staticMetaObject, qt_meta_stringdata_SeparateComment,
      qt_meta_data_SeparateComment, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SeparateComment::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SeparateComment::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SeparateComment::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SeparateComment))
        return static_cast<void*>(const_cast< SeparateComment*>(this));
    return DisplayBase::qt_metacast(_clname);
}

int SeparateComment::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = DisplayBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_SingleEntryEdit[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SingleEntryEdit[] = {
    "SingleEntryEdit\0\0OnEditChange()\0"
};

void SingleEntryEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SingleEntryEdit *_t = static_cast<SingleEntryEdit *>(_o);
        switch (_id) {
        case 0: _t->OnEditChange(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SingleEntryEdit::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SingleEntryEdit::staticMetaObject = {
    { &SeparateComment::staticMetaObject, qt_meta_stringdata_SingleEntryEdit,
      qt_meta_data_SingleEntryEdit, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SingleEntryEdit::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SingleEntryEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SingleEntryEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SingleEntryEdit))
        return static_cast<void*>(const_cast< SingleEntryEdit*>(this));
    return SeparateComment::qt_metacast(_clname);
}

int SingleEntryEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SeparateComment::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_SingleEntryButton[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SingleEntryButton[] = {
    "SingleEntryButton\0\0OnButtonClick()\0"
};

void SingleEntryButton::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SingleEntryButton *_t = static_cast<SingleEntryButton *>(_o);
        switch (_id) {
        case 0: _t->OnButtonClick(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SingleEntryButton::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SingleEntryButton::staticMetaObject = {
    { &SingleEntryEdit::staticMetaObject, qt_meta_stringdata_SingleEntryButton,
      qt_meta_data_SingleEntryButton, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SingleEntryButton::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SingleEntryButton::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SingleEntryButton::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SingleEntryButton))
        return static_cast<void*>(const_cast< SingleEntryButton*>(this));
    return SingleEntryEdit::qt_metacast(_clname);
}

int SingleEntryButton::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SingleEntryEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_SingleEntryInputFile[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_SingleEntryInputFile[] = {
    "SingleEntryInputFile\0"
};

void SingleEntryInputFile::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SingleEntryInputFile::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SingleEntryInputFile::staticMetaObject = {
    { &SingleEntryButton::staticMetaObject, qt_meta_stringdata_SingleEntryInputFile,
      qt_meta_data_SingleEntryInputFile, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SingleEntryInputFile::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SingleEntryInputFile::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SingleEntryInputFile::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SingleEntryInputFile))
        return static_cast<void*>(const_cast< SingleEntryInputFile*>(this));
    return SingleEntryButton::qt_metacast(_clname);
}

int SingleEntryInputFile::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SingleEntryButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_SingleEntryOutputFile[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_SingleEntryOutputFile[] = {
    "SingleEntryOutputFile\0"
};

void SingleEntryOutputFile::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SingleEntryOutputFile::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SingleEntryOutputFile::staticMetaObject = {
    { &SingleEntryButton::staticMetaObject, qt_meta_stringdata_SingleEntryOutputFile,
      qt_meta_data_SingleEntryOutputFile, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SingleEntryOutputFile::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SingleEntryOutputFile::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SingleEntryOutputFile::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SingleEntryOutputFile))
        return static_cast<void*>(const_cast< SingleEntryOutputFile*>(this));
    return SingleEntryButton::qt_metacast(_clname);
}

int SingleEntryOutputFile::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SingleEntryButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_SingleEntryDirectory[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_SingleEntryDirectory[] = {
    "SingleEntryDirectory\0"
};

void SingleEntryDirectory::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SingleEntryDirectory::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SingleEntryDirectory::staticMetaObject = {
    { &SingleEntryButton::staticMetaObject, qt_meta_stringdata_SingleEntryDirectory,
      qt_meta_data_SingleEntryDirectory, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SingleEntryDirectory::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SingleEntryDirectory::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SingleEntryDirectory::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SingleEntryDirectory))
        return static_cast<void*>(const_cast< SingleEntryDirectory*>(this));
    return SingleEntryButton::qt_metacast(_clname);
}

int SingleEntryDirectory::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SingleEntryButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_SingleEntryColor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_SingleEntryColor[] = {
    "SingleEntryColor\0"
};

void SingleEntryColor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SingleEntryColor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SingleEntryColor::staticMetaObject = {
    { &SingleEntryButton::staticMetaObject, qt_meta_stringdata_SingleEntryColor,
      qt_meta_data_SingleEntryColor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SingleEntryColor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SingleEntryColor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SingleEntryColor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SingleEntryColor))
        return static_cast<void*>(const_cast< SingleEntryColor*>(this));
    return SingleEntryButton::qt_metacast(_clname);
}

int SingleEntryColor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SingleEntryButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_List[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_List[] = {
    "List\0"
};

void List::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData List::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject List::staticMetaObject = {
    { &SingleEntryEdit::staticMetaObject, qt_meta_stringdata_List,
      qt_meta_data_List, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &List::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *List::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *List::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_List))
        return static_cast<void*>(const_cast< List*>(this));
    return SingleEntryEdit::qt_metacast(_clname);
}

int List::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SingleEntryEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_Matrix[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x08,
      28,    7,    7,    7, 0x08,
      48,    7,    7,    7, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Matrix[] = {
    "Matrix\0\0OnEditButtonClick()\0"
    "OnLoadButtonClick()\0OnSaveButtonClick()\0"
};

void Matrix::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Matrix *_t = static_cast<Matrix *>(_o);
        switch (_id) {
        case 0: _t->OnEditButtonClick(); break;
        case 1: _t->OnLoadButtonClick(); break;
        case 2: _t->OnSaveButtonClick(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData Matrix::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Matrix::staticMetaObject = {
    { &SeparateComment::staticMetaObject, qt_meta_stringdata_Matrix,
      qt_meta_data_Matrix, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Matrix::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Matrix::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Matrix::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Matrix))
        return static_cast<void*>(const_cast< Matrix*>(this));
    return SeparateComment::qt_metacast(_clname);
}

int Matrix::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SeparateComment::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_SingleEntryEnum[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_SingleEntryEnum[] = {
    "SingleEntryEnum\0"
};

void SingleEntryEnum::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SingleEntryEnum::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SingleEntryEnum::staticMetaObject = {
    { &SeparateComment::staticMetaObject, qt_meta_stringdata_SingleEntryEnum,
      qt_meta_data_SingleEntryEnum, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SingleEntryEnum::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SingleEntryEnum::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SingleEntryEnum::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SingleEntryEnum))
        return static_cast<void*>(const_cast< SingleEntryEnum*>(this));
    return SeparateComment::qt_metacast(_clname);
}

int SingleEntryEnum::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SeparateComment::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_SingleEntryBoolean[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_SingleEntryBoolean[] = {
    "SingleEntryBoolean\0"
};

void SingleEntryBoolean::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SingleEntryBoolean::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SingleEntryBoolean::staticMetaObject = {
    { &DisplayBase::staticMetaObject, qt_meta_stringdata_SingleEntryBoolean,
      qt_meta_data_SingleEntryBoolean, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SingleEntryBoolean::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SingleEntryBoolean::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SingleEntryBoolean::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SingleEntryBoolean))
        return static_cast<void*>(const_cast< SingleEntryBoolean*>(this));
    return DisplayBase::qt_metacast(_clname);
}

int SingleEntryBoolean::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = DisplayBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
