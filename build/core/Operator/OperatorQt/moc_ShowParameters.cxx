/****************************************************************************
** Meta object code from reading C++ file 'ShowParameters.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/core/Operator/OperatorQt/ShowParameters.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ShowParameters.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ShowParameters[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      25,   15,   15,   15, 0x08,
      38,   15,   15,   15, 0x08,
      51,   15,   15,   15, 0x08,
      70,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ShowParameters[] = {
    "ShowParameters\0\0OnShow()\0OnAccepted()\0"
    "OnRejected()\0OnExpandCollapse()\0"
    "OnItemChanged()\0"
};

void ShowParameters::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ShowParameters *_t = static_cast<ShowParameters *>(_o);
        switch (_id) {
        case 0: _t->OnShow(); break;
        case 1: _t->OnAccepted(); break;
        case 2: _t->OnRejected(); break;
        case 3: _t->OnExpandCollapse(); break;
        case 4: _t->OnItemChanged(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ShowParameters::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ShowParameters::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ShowParameters,
      qt_meta_data_ShowParameters, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ShowParameters::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ShowParameters::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ShowParameters::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ShowParameters))
        return static_cast<void*>(const_cast< ShowParameters*>(this));
    return QDialog::qt_metacast(_clname);
}

int ShowParameters::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
