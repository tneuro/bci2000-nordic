/****************************************************************************
** Meta object code from reading C++ file 'SysLog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/core/Operator/OperatorQt/SysLog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SysLog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SysLog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   13,    8,    7, 0x0a,
      31,    7,    8,    7, 0x2a,
      45,   39,    7,    7, 0x0a,
      72,   67,    7,    7, 0x2a,

       0        // eod
};

static const char qt_meta_stringdata_SysLog[] = {
    "SysLog\0\0bool\0force\0Close(bool)\0Close()\0"
    "text,\0AddEntry(QString,int)\0text\0"
    "AddEntry(QString)\0"
};

void SysLog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SysLog *_t = static_cast<SysLog *>(_o);
        switch (_id) {
        case 0: { bool _r = _t->Close((*reinterpret_cast< bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 1: { bool _r = _t->Close();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: _t->AddEntry((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->AddEntry((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SysLog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SysLog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_SysLog,
      qt_meta_data_SysLog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SysLog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SysLog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SysLog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SysLog))
        return static_cast<void*>(const_cast< SysLog*>(this));
    return QDialog::qt_metacast(_clname);
}

int SysLog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
