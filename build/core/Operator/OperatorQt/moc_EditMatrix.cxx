/****************************************************************************
** Meta object code from reading C++ file 'EditMatrix.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/core/Operator/OperatorQt/EditMatrix.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'EditMatrix.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EditMatrix[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      38,   11,   11,   11, 0x08,
      48,   11,   11,   11, 0x08,
      72,   11,   11,   11, 0x08,
     103,   11,   11,   11, 0x08,
     119,   11,   11,   11, 0x08,
     135,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_EditMatrix[] = {
    "EditMatrix\0\0OnChangeMatrixSizeClick()\0"
    "OnClose()\0ShowContextMenu(QPoint)\0"
    "PerformContextAction(QAction*)\0"
    "EditHLabel(int)\0EditVLabel(int)\0"
    "EditItem(QTableWidgetItem*)\0"
};

void EditMatrix::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        EditMatrix *_t = static_cast<EditMatrix *>(_o);
        switch (_id) {
        case 0: _t->OnChangeMatrixSizeClick(); break;
        case 1: _t->OnClose(); break;
        case 2: _t->ShowContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 3: _t->PerformContextAction((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 4: _t->EditHLabel((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->EditVLabel((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->EditItem((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData EditMatrix::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject EditMatrix::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_EditMatrix,
      qt_meta_data_EditMatrix, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EditMatrix::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EditMatrix::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EditMatrix::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EditMatrix))
        return static_cast<void*>(const_cast< EditMatrix*>(this));
    return QDialog::qt_metacast(_clname);
}

int EditMatrix::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
