# Install script for directory: C:/BCI2000/BCI2000/src/contrib/SignalSource

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/BCI2000")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/AmpServerPro/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/BioRadio/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/Biosemi/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/B-Alert/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/DASSource/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/Emotiv/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/Enobio/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/gHIampSource/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/Micromed/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/ModularEEG/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/Neuralynx/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/Neuroscan/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/NeuroscanAccessSDK/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/NeuroSky/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/NIDAQ-MX/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/RDAClient/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/Ripple/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/TMSi/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/TuckerDavis/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/vAmp/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/FieldTripBufferSource/cmake_install.cmake")
  INCLUDE("C:/BCI2000/BCI2000/build/contrib/SignalSource/FilePlayback/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

