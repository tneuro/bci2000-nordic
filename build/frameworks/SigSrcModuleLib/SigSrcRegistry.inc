// Generated file -- do not edit
RegisterFilter( NullFileWriter, 1 );
RegisterFilter( BCI2000FileWriter, 1 );
RegisterFilter( EDFFileWriter, 1 );
RegisterFilter( GDFFileWriter, 1 );
RegisterFilter( AlignmentFilter, 1.1 );
RegisterFilter( DataIOFilter, 0 );
RegisterFilter( TransmissionFilter, 1.2 );
Extension( EventLink );
Extension( JoystickLogger );
Extension( KeyLogger );
Extension( DataGloveLogger );
Extension( WebcamLogger );
Extension( EyetrackerLogger );
Extension( WiimoteLogger );
Extension( AudioExtension );
