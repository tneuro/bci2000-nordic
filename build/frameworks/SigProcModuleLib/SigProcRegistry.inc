// Generated file -- do not edit
RegisterFilter( SpatialFilter, 2.B );
RegisterFilter( IIRBandpass, 2.C1 );
RegisterFilter( SpectralEstimator, 2.C );
RegisterFilter( ARFilter, 2.C );
RegisterFilter( P3TemporalFilter, 2.D );
RegisterFilter( LinearClassifier, 2.D );
RegisterFilter( LPFilter, 2.B1 );
RegisterFilter( ExpressionFilter, 2.D2 );
RegisterFilter( Normalizer, 2.E1 );
RegisterFilter( AverageDisplay, 2.C1 );
RegisterFilter( RandomFilter, 2.C1 );
